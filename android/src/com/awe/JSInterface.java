package com.awe;

import android.database.Cursor;
import android.webkit.JavascriptInterface;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class JSInterface {
	
	static public JSONArray posts = new JSONArray();
	
	@JavascriptInterface
	public String toString() { 
		
		Cursor c = AWEActivity.db.rawQuery("SELECT * FROM " + DBHelper.TABLE + " ORDER BY "+DBHelper.C_DATE + " LIMIT 50", null);
		
		if (c != null) {
		    if (c.moveToFirst()) {
		        do {
		        	
		        	JSONObject post = new JSONObject();
		        	
		            String title = c.getString(c.getColumnIndex(DBHelper.C_TITLE));
		            String guid = c.getString(c.getColumnIndex(DBHelper.C_GUID));
		            String permalink = c.getString(c.getColumnIndex(DBHelper.C_PERMALINK));
		            String body = c.getString(c.getColumnIndex(DBHelper.C_BODY));
		            String date = c.getString(c.getColumnIndex(DBHelper.C_DATE));

		        	post.put("title", title);
		        	post.put("guid", guid);
		        	post.put("permalink", permalink);
		        	post.put("body", body);
		        	post.put("date", date);
		        	
		        	posts.add( post );
		        	
		        } while ( c.moveToNext() );
		    }
		}
		
		return posts.toJSONString();
	}
}

package com.awe;

public class FeedRunnable implements Runnable {
	
	String url;
	FeedRunnable(String s) { url = s; }

	@Override
	public void run() {
		FeedParser feedparser = new FeedParser();
		feedparser.save( url );
	}

}

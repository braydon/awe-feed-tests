<?php

include_once('simplepie/autoloader.php');

// First, include Requests
include('Requests/library/Requests.php');

// Next, make sure Requests can load internal classes
Requests::register_autoloader();

$sources = json_decode( file_get_contents( 'sources.json' ) );

$requests = array();
$keys = array();

foreach ( $sources as $key => $source ) {
	$keys[] = $source;
	$requests[] = array( 
		'url' => $source->url
	);
}

$bad_chrs = array(
	chr(0), //null
	chr(1), //start of heading
	chr(2), //start of text
	chr(3), //end of text
	chr(4), //end of transmission
	chr(5), //enquiry
	chr(6), //acknowledge
	chr(7), //bell
	chr(8), //backspace
	chr(10), //linefeed
	chr(11), //vertical tabulation
	chr(12), //form feed
	chr(13), //carriage return
	chr(14), //shift out
	chr(15), //shift in
	chr(16), //data link escape
	chr(17), //device control
	chr(18), //device control two
	chr(19), //device control three
	chr(20), //device controll four
	chr(21), //negative acknowledge
	chr(22), //synchronous idle
	chr(23), //end of transmission block
	chr(24), //cancel
	chr(25), //end of medium
	chr(26), //substitute
	chr(27), //escape
	chr(28), //file separator
	chr(29), //group separator
	chr(30), //record separator
	chr(31), //unit separator
	chr(127), //delete
);
 


// Setup a callback
function process_request_callback(&$request, $id) {
	global $bad_chrs;
	global $keys;
	if ( get_class( $request ) == 'Requests_Exception' ) {
		print 'Exception for '.$keys[$id]->title.':' .$request->getType().  PHP_EOL;
	} else {
		if ( $request->status_code == '200' ) {
			$feed = new SimplePie();
            $body = str_replace( $bad_chrs, "", $request->body );
			$feed->set_raw_data( $body );
			$feed->init();
			$feed->handle_content_type();
			print 'New Items for '.$keys[$id]->title.': '.$feed->get_item_quantity().  PHP_EOL;
		} else if ( $request->status_code == '404' ) {
			print 'Not Found for '.$keys[$id]->title. PHP_EOL;
		} else {
			print 'Status for '.$keys[$id]->title.': ' .$request->status_code. PHP_EOL;
		}
	}
}

// Tell Requests to use the callback
$options = array(
	'complete' => 'process_request_callback',
	'timeout' => 120
);

// Send the request!
$responses = Requests::request_multiple($requests, $options);

?>
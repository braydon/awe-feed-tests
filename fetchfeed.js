var feedparser = require('feedparser')
var fs = require('fs')
var request = require('request')

function callback (article) {
	//console.log('Got article: %s', JSON.stringify(article));
	articles = articles+1;
	console.log('Got article: %s', article['title']);
	status();
}

var sources = JSON.parse(fs.readFileSync('sources.json', 'utf8'))

var article_errors = 0;
var feed_errors = 0;
var articles = 0;
var feeds = 0;

var l = sources.length;
for ( var key in sources ) {
	var req = {'uri': sources[key]['url'] };
	request(req, function (error, response, body){
		if (!error && response.statusCode == 200) {
			feeds = feeds+1;
			var content_type = response.headers['content-type'];
			feedparser.parseString(body).on('article', callback).on('error', function(e){
				article_errors = article_errors+1;
				console.log(e);
				status();
			});
		} else {
			if ( response != null && response.hasOwnProperty('statusCode') ) {
				console.log( response.statusCode )
			}
			console.log(error)
			feed_errors = feed_errors+1;
			status();
		}
	});
}

function status (){
	console.log('Article Errors:' + article_errors );
	console.log('Feed Errors:' + feed_errors );
	console.log('Feeds:' + feeds );
	console.log('Articles:' + articles );
}

//var reqObj = {'uri': 'http://cyber.law.harvard.edu/rss/examples/rss2sample.xml',
//              'headers': {'If-Modified-Since' : <your cached 'lastModified' value>,
//                          'If-None-Match' : <your cached 'etag' value>}};





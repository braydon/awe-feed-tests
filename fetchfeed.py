import os, sys, json, threading, feedparser, Queue

with open('sources.json', 'r') as f:
    sources = json.loads(f.read())

queue = Queue.Queue()

class ThreadFetchFeed(threading.Thread):
    """Threaded Url Grab"""
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            source = self.queue.get()

            feed = feedparser.parse( source['url'] )
            if feed.bozo:
                raise feed.bozo_exception
            
            #print feed
            self.queue.task_done()


def main():
    for i in range(5):
        t = ThreadFetchFeed(queue)
        t.setDaemon(True)
        t.start()

    for key in sources:
        queue.put(sources[key])

    queue.join()


main()


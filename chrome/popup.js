var awe = {};
awe.indexedDB = {};
awe.indexedDB.db = null;

awe.indexedDB.open = function() {
	var version = 2;
	var request = indexedDB.open( "awe", version );

	request.onsuccess = function(e) {
		awe.indexedDB.db = e.target.result;
		awe.indexedDB.getPosts();
	};

	request.onerror = awe.indexedDB.onerror;
};

awe.indexedDB.getPosts = function() {

	var db = awe.indexedDB.db;
	var trans = db.transaction(["posts"], "readwrite");
	var store = trans.objectStore("posts");

	// Get everything in the store;
	var key_range = IDBKeyRange.lowerBound(0);
	var cursor_request = store.openCursor(key_range);

	cursor_request.onsuccess = function(e) {
		var result = e.target.result;
		if(!!result == false)
			return;

		document.write("<h2><a target='_blank' href='"+result.value['permalink']+"'>"+result.value['title']+"</a></h2>");
		if ( result.value['content'] != '' ) {
			document.write("<div>"+result.value['content']+"</div>");
		} else {
			document.write("<div>"+result.value['description']+"</div>");
		}
		result.continue();
	};

	cursor_request.onerror = awe.indexedDB.onerror;
};


document.addEventListener('DOMContentLoaded', function () {
	awe.indexedDB.open();
});

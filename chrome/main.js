var awe = {};
awe.indexedDB = {};
awe.indexedDB.db = null;
awe.window = {};

awe.window.open = function(first) {
	var url = "popup.html";

	var fullUrl = chrome.extension.getURL(url);

	chrome.tabs.getAllInWindow(null, function(tabs) {
		for (var i in tabs) { // check if Options page is open already
			var tab = tabs[i];
			if (tab.url == fullUrl || tab.url.substring(0, fullUrl.length) == fullUrl) {
				chrome.tabs.update(tab.id, { selected: true }); // select the tab
				return;
			}
		}
		chrome.tabs.getSelected(null, function(tab) { // open a new tab next to currently selected tab
			chrome.tabs.create({
				url: url,
				pinned: true,
				index: tab.index + 1
			});
		});
	});
}

awe.indexedDB.fetchFeeds = function(){
	for ( var key in sources ) {
		awe.indexedDB.fetchFeed( sources[key]['url'], new XMLHttpRequest() );
	}
}

awe.indexedDB.fetchFeed = function(url, xhr){
	xhr.onreadystatechange = function(){
		if ( xhr.readyState == 4 && xhr.status == 200 ) {
			var doc = xhr.responseXML;
			if (!doc) {
				console.log( 'Error:'+ url )
				//console.log( xhr.responseText )
				return;
			}

			var entries = doc.getElementsByTagName('entry');
			if (entries.length == 0) {
				entries = doc.getElementsByTagName('item');
			}
			var count = entries.length;

			for (var i = 0; i < count; i++) {
				var item = entries.item(i);
				awe.indexedDB.savePost(item);

			}
		}
	}
	xhr.open('GET', url, true);
	xhr.send();
}

awe.indexedDB.open = function() {
	var version = 2;
	var request = indexedDB.open( "awe", version );

	request.onsuccess = function(e) {
		awe.indexedDB.db = e.target.result;
		fetch_feeds();
	};

	// We can only create Object stores in a versionchange transaction.
	request.onupgradeneeded = function(e) {
		var db = e.target.result;

	    // A versionchange transaction is started automatically.
		e.target.transaction.onerror = awe.indexedDB.onerror;

		if ( db.objectStoreNames.contains( "posts" ) ) {
			db.deleteObjectStore( "posts" );
		}

		var store = db.createObjectStore( "posts", { keyPath: "guid" } );
	};

	request.onerror = awe.indexedDB.onerror;
};

awe.indexedDB.savePost = function( item ) {
	var db = awe.indexedDB.db;
	var trans = db.transaction(["posts"], "readwrite");
	var store = trans.objectStore("posts");
	var guid = item.querySelector('guid');

	if ( guid ) {

		guid = guid.textContent;

		// Grab the title for the feed item.
		var title = item.querySelector('title');
		if (title) {
			title = title.textContent;
		} else {
			title = false;
		}

		// Grab the description.
		var description = item.querySelector('description');
		if (!description) {
			description = item.querySelector('summary');
		}

		if (description) {
			try {
				description = description.childNodes[0].nodeValue;
			} catch(e) {
				console.log(e)
				description = '';
			}
		} else {
			description = '';
		}

		// Grab the content.
		var content = item.querySelector('content');

		if (content) {
			try {
				content = content.childNodes[0].nodeValue;
			} catch(e) {
				console.log(content.textContent)
				content = '';
			}
		} else {
			content = '';
		}

		//var date = item.querySelector('pubDate')
		var permalink = item.querySelector('link');
		if (permalink) {
			permalink = permalink.textContent;
		} else {
			permalink = false;
		}

		if ( permalink ) {

			var request = store.put({
				"guid" : guid,
				"title": title,
				"date" : new Date().getTime(),
				"permalink" : permalink,
				"description" : description,
				"content" : content
			});

			request.onsuccess = function(e) {
				// neat
			};

			request.onerror = function(e) {
				console.log(e.value);
			};

		} else {
			console.log( 'Error Saving: No permalink' );
		}

	} else {
		console.log('Error Saving: No guid')
	}

};

awe.indexedDB.open();
awe.window.open();


//var reqObj = {'uri': 'http://cyber.law.harvard.edu/rss/examples/rss2sample.xml',
//              'headers': {'If-Modified-Since' : <your cached 'lastModified' value>,
//                          'If-None-Match' : <your cached 'etag' value>}};





package main

import (
	"fmt"
	"io/ioutil"
	"encoding/json"
	"os"
	"sync"
	rss "github.com/jteeuwen/go-pkg-rss"
)

type FeedSource struct {
    Slug	string
	Url		string
	Title	string
	Reader	string
	User	float64
}

var sources map[string]FeedSource

func main() {

	var wg sync.WaitGroup

	file, e := ioutil.ReadFile("./sources.json")

	if e != nil {
		fmt.Println("File error: %v\n", e)
		os.Exit(1)
	}   

    je := json.Unmarshal(file, &sources)

	if je != nil {
		fmt.Println("JSON error:", je)
	}

	for key := range sources {
		fmt.Printf( key )
		wg.Add(1)
		go func( source FeedSource ) {
			fmt.Printf( source.Url )
			feed := rss.New(5, true, chanHandler, itemHandler)
			if err := feed.Fetch(source.Url, nil); err != nil {
				fmt.Fprintf(os.Stderr, "[e] %s: %s", source.Url, err)
				return
			}
			wg.Done()
		}( sources[key] )
	}
	wg.Wait()
}

func chanHandler(feed *rss.Feed, newchannels []*rss.Channel) {
	fmt.Printf("%d new channel(s) in %s\n", len(newchannels), feed.Url)
}

func itemHandler(feed *rss.Feed, ch *rss.Channel, newitems []*rss.Item) {
	fmt.Printf("%d new item(s) in %s\n", len(newitems), feed.Url)
}